<?php
  header('Content-Type: application/json');
  header('Access-Control-Allow-Origin: *');
    
  require(__DIR__ . '/config.php');
  
  require_once(__DIR__ . '/lib/Mysql/Statement.php');
  require_once(__DIR__ . '/lib/Mysql/Exception.php');
  require_once(__DIR__ . '/lib/Mysql/Mysql.php');

  use Krugozor\Database\Mysql\Mysql as Mysql;

  $db = Mysql::create($opts['host'], $opts['login'], $opts['password'])
      // Выбор базы данных
      ->setDatabaseName($opts['db'])
      // Выбор кодировки
      ->setCharset("utf8");
  
  $yearFrom = '2014';
  $yearTo = '2019';

  if (isset($_GET['year-from']) && isset($_GET['year-to'])) {
    $yearFrom = $_GET['year-from'];
    $yearTo = $_GET['year-to'];
  }

  $data = array();
  $fields = array();


  function getAccident($db, $yearFrom, $yearTo, $regionId = 0, $data = array()) {

    $dopSql = '';
    
    if ($regionId > 0) {
      $dopSql = 't1.region_id = ' . addslashes($regionId) . ' AND ';
    }

    $res = $db->query(
      "SELECT 
      t.region_id,
        t.region_name,
        t.year,
        sum(t.victims_amount) as victims_amount,
        sum(t.vehicles_amount) as vehicles_amount,
        sum(t.participants_amount) as participants_amount
    FROM (
    SELECT 
      t1.region_id,
        t1.region_name, 
        t1.year, 
        t2.value as victims_amount, 
        t3.value as vehicles_amount,
        t4.value as participants_amount
    FROM 
      `problems` t1, 
        `indexes` t2,
        `indexes` t3,
        `indexes` t4
    WHERE 
      " . $dopSql . "
      t1.type_id = 1 AND 
        t1.year >= ?i AND 
        t1.year <= ?i AND 
        t2.problem_id = t1.id AND
        t3.problem_id = t1.id AND
        t4.problem_id = t1.id AND
        t2.index_id = 1 AND
        t3.index_id = 2 AND
        t4.index_id = 3    
    ) t
    GROUP BY t.region_id, t.region_name, t.year
    ORDER BY `t`.`victims_amount`  DESC",
      $yearFrom,
      $yearTo
    );

    if ($res->getNumRows() > 0) {
      while ($row = $res->fetch_assoc()) {
        
        $color = 4;

        if ($row['victims_amount'] > 150) {
          $color = 1;
        } else if($row['victims_amount'] > 100 && $row['victims_amount'] <= 150) {
          $color = 2;
        } else if($row['victims_amount'] > 50  && $row['victims_amount'] <= 100) {
          $color = 3;
        }

        $row['color'] = $color;

        array_push($data, $row);
      }
    }
    
    $fields = array(
      'region_id' => array(
        'label' => 'Id региона',
        'unit' => ''
      ),
      'region_name' => array(
        'label' => 'Регион',
        'unit' => ''
      ),
      'year' => array(
        'label' => 'Год',
        'unit' => ''
      ),
      'victims_amount' => array(
        'label' => 'Количество погибших',
        'unit' => 'чел.'
      ),
      'vehicles_amount'=> array(
        'label' => 'Количество ТС',
        'unit' => 'шт.'
      ),
      'participants_amount' => array(
        'label' => 'Количество участников',
        'unit' => 'чел.'
      ),
      'color' => array(
        'label' => 'Цвет',
        'unit' => ''
      )
    );

    return array(
      'data' => $data,
      'fields' => $fields
    );
  }

  //ДТП
  if ($_GET['type'] == 'accident') {
    $res = getAccident($db, $yearFrom, $yearTo);

    $data = $res['data'];
    $fields = $res['fields'];
  }

  function getCrime($db, $yearFrom, $yearTo, $regionId = 0, $data = array()) {
    $dopSql = '';
    
    if ($regionId > 0) {
      $dopSql = 't1.region_id = ' . addslashes($regionId) . ' AND ';
    }

    $res = $db->query(
      "SELECT 
        t1.region_name,  
        t1.year, 
        t2.value 
      FROM 
        `problems` t1, 
        `indexes` t2 
      WHERE 
        " . $dopSql . "
        t1.type_id = 2 AND 
        t1.year >= ?i AND 
        t1.year <= ?i AND 
        t2.problem_id = t1.id 
      order by t2.value desc",
      $yearFrom,
      $yearTo
    );

    if ($res->getNumRows() > 0) {
      while ($row = $res->fetch_assoc()) {

        $color = 4;

        if ($row['value'] > 200) {
          $color = 1;
        } else if($row['value'] > 125  && $row['value'] <= 200) {
          $color = 2;
        } else if($row['value'] > 50  && $row['value'] <= 125) {
          $color = 3;
        }

        $row['color'] = $color;

        array_push($data, $row);
      }
    }

    
    $fields = array(
      'region_name' => array(
        'label' => 'Регион',
        'unit' => ''
      ),
      'year' => array(
        'label' => 'Год',
        'unit' => ''
      ),
      'value' => array(
        'label' => 'Количество преступлений',
        'unit' => 'шт.'
      )
    );

    return array(
      'data' => $data,
      'fields' => $fields
    );
  }

  //Преступления
  if ($_GET['type'] == 'crime') {

    $res = getCrime($db, $yearFrom, $yearTo);

    $data = $res['data'];
    $fields = $res['fields'];
    
  }

  function getDemography($db, $yearFrom, $yearTo, $regionId = 0, $data = array()) {

    $dopSql = '';
    
    if ($regionId > 0) {
      $dopSql = 't1.region_id = ' . addslashes($regionId) . ' AND ';
    }

    $res = $db->query(
      "SELECT 
        t1.region_name,  
        t1.year, 
        t2.value 
      FROM 
        `problems` t1, 
        `indexes` t2 
      WHERE 
        " . $dopSql . "
        t1.type_id = 3 AND 
        t1.year >= ?i AND 
        t1.year <= ?i AND 
        t2.problem_id = t1.id 
      order by t2.value desc",
      $yearFrom,
      $yearTo
    );

    if ($res->getNumRows() > 0) {
      while ($row = $res->fetch_assoc()) {

        $color = 4;

        if ($row['value'] > 85) {
          $color = 1;
        } else if($row['value'] > 70  && $row['value'] <= 85) {
          $color = 2;
        } else if($row['value'] > 50  && $row['value'] <= 70) {
          $color = 3;
        }

        $row['color'] = $color;

        array_push($data, $row);
      }
    }

    $fields = array(
      'region_name' => array(
        'label' => 'Регион',
        'unit' => ''
      ),
      'year' => array(
        'label' => 'Год',
        'unit' => ''
      ),
      'value' => array(
        'label' => 'Коэффициент',
        'unit' => ''
      ),
      'color' => array(
        'label' => 'Цвет',
        'unit' => ''
      )
    );

    return array(
      'data' => $data,
      'fields' => $fields
    );
  }
  
  //Демография
  if ($_GET['type'] == 'demography') {
    $res = getDemography($db, $yearFrom, $yearTo);

    $data = $res['data'];
    $fields = $res['fields'];
  }

  function getEco($db, $yearFrom, $yearTo, $regionId = 0, $data = array()) {

    $dopSql = '';
    
    if ($regionId > 0) {
      $dopSql = 't1.region_id = ' . addslashes($regionId) . ' AND ';
    }

    $res = $db->query(
      "SELECT 
      t.region_name,
      t.year,
      sum(t.raiting) as raiting,
      sum(t.sv_index) as sv_index,
      sum(t.pr_index) as pr_index,
      sum(t.prom_index) as prom_index,
      sum(t.soc_index) as soc_index
  FROM (
  SELECT 
      t1.region_name, 
      t1.year, 
      t2.value as raiting, 
      t3.value as sv_index,
      t4.value as pr_index,
      t5.value as prom_index,
      t6.value as soc_index 
  FROM 
    `problems` t1, 
      `indexes` t2,
      `indexes` t3,
      `indexes` t4,
      `indexes` t5,
      `indexes` t6
  WHERE 
    " . $dopSql . "
    t1.type_id = 4 AND 
      t1.year >= ?i AND 
      t1.year <= ?i AND 
      t2.problem_id = t1.id AND
      t3.problem_id = t1.id AND
      t4.problem_id = t1.id AND
      t5.problem_id = t1.id AND
      t6.problem_id = t1.id AND
      t2.index_id = 1 AND
      t3.index_id = 2 AND
      t4.index_id = 3 AND
      t5.index_id = 4 AND
      t6.index_id = 5
  ) t
  GROUP BY t.region_name, t.year
  ORDER BY `t`.`raiting`  DESC",
      $yearFrom,
      $yearTo
    );

    if ($res->getNumRows() > 0) {
      while ($row = $res->fetch_assoc()) {

        $color = 4;

        if ($row['raiting'] > 70) {
          $color = 1;
        } else if($row['raiting'] > 50 &&  $row['raiting'] <= 70) {
          $color = 2;
        } else if($row['raiting'] > 25  && $row['raiting'] <= 50) {
          $color = 3;
        }

        $row['color'] = $color;


        array_push($data, $row);
      }
    }

    $fields = array(
      'region_name' => array(
        'label' => 'Регион',
        'unit' => ''
      ),
      'raiting' => array(
        'label' => 'Место в рейтинге',
        'unit' => ''
      ),
      'sv_index' => array(
        'label' => 'Сводный индекс',
        'unit' => ''
      ),
      'pr_index' => array(
        'label' => 'Природоохранный индекс',
        'unit' => ''
      ),
      'prom_index' => array(
        'label' => 'Промышленный индекс',
        'unit' => ''
      ),
      'soc_index' => array(
        'label' => 'Социально-экономический индекс',
        'unit' => ''
      ),
      'color' => array(
        'label' => 'Цвет',
        'unit' => ''
      )
    );

    return array(
      'data' => $data,
      'fields' => $fields
    );
  }

  //Экология
  if ($_GET['type'] == 'eco') {
    $res = getEco($db, $yearFrom, $yearTo);

    $data = $res['data'];
    $fields = $res['fields'];    
  }

  function getSalary($db, $yearFrom, $yearTo, $regionId = 0, $data = array()) {

    $dopSql = '';
    
    if ($regionId > 0) {
      $dopSql = 't1.region_id = ' . addslashes($regionId) . ' AND ';
    }

    $res = $db->query(
      "SELECT 
        t1.region_name,  
        t1.year, 
        t2.value 
      FROM 
        `problems` t1, 
        `indexes` t2 
      WHERE 
        " . $dopSql . "
        t1.type_id = 5 AND 
        t1.year >= ?i AND 
        t1.year <= ?i AND 
        t2.problem_id = t1.id 
      order by t1.year asc",
      $yearFrom,
      $yearTo
    );

    if ($res->getNumRows() > 0) {
      while ($row = $res->fetch_assoc()) {
        array_push($data, $row);
      }
    }

    $fields = array(
      'region_name' => array(
        'label' => 'Регион',
        'unit' => ''
      ),
      'year' => array(
        'label' => 'Год',
        'unit' => ''
      ),
      'value' => array(
        'label' => 'Средняя зарплата',
        'unit' => 'руб.'
      )
    );

    return array(
      'data' => $data,
      'fields' => $fields
    );

  }

  //Средняя зарплата
  if ($_GET['type'] == 'salary') {
    $res = getSalary($db, $yearFrom, $yearTo);

    $data = $res['data'];
    $fields = $res['fields'];     
  }

  function getEstate($db, $yearFrom, $yearTo, $regionId = 0, $data = array()) {

    $dopSql = '';
    
    if ($regionId > 0) {
      $dopSql = 't1.region_id = ' . addslashes($regionId) . ' AND ';
    }

    $res = $db->query(
      "SELECT 
        t1.region_name,  
        t1.year, 
        t2.value 
      FROM 
        `problems` t1, 
        `indexes` t2 
      WHERE 
        " . $dopSql . "
        t1.type_id = 6 AND 
        t1.year >= ?i AND 
        t1.year <= ?i AND 
        t2.problem_id = t1.id 
      order by t1.year asc",
      $yearFrom,
      $yearTo
    );

    if ($res->getNumRows() > 0) {
      while ($row = $res->fetch_assoc()) {
        array_push($data, $row);
      }
    }

    $fields = array(
      'region_name' => array(
        'label' => 'Регион',
        'unit' => ''
      ),
      'year' => array(
        'label' => 'Год',
        'unit' => ''
      ),
      'value' => array(
        'label' => 'Стоимость',
        'unit' => 'тыс. руб. за кв.м.'
      )
    );

    return array(
      'data' => $data,
      'fields' => $fields
    );
  }

  //Средняя цена за квадратный метр
  if ($_GET['type'] == 'real-estate') {
    
    $res = getEstate($db, $yearFrom, $yearTo);

    $data = $res['data'];
    $fields = $res['fields'];    
    
  }

  if ($_GET['type'] == 'simple-region' && isset($_GET['region-id'])) {
    $resAcc = getAccident($db, $yearFrom, $yearTo, $_GET['region-id']);
    $resCrime = getCrime($db, $yearFrom, $yearTo, $_GET['region-id']);
    $resDemography = getDemography($db, $yearFrom, $yearTo, $_GET['region-id']);
    $resEco = getEco($db, $yearFrom, $yearTo, $_GET['region-id']);
    $resSalary = getSalary($db, $yearFrom, $yearTo, $_GET['region-id']);
    $resEstate = getEstate($db, $yearFrom, $yearTo, $_GET['region-id']);

    $data = array(
      'accident' => $resAcc['data'],
      'crime' => $resCrime['data'],
      'demography' => $resDemography['data'],
      'eco' => $resEco['data'],
      'salary' => $resSalary['data'],
      'estate' => $resEstate['data'],
    );

    $fields = array(
      'accident' => $resAcc['fields'],
      'crime' => $resCrime['fields'],
      'demography' => $resDemography['fields'],
      'eco' => $resEco['fields'],
      'salary' => $resSalary['fields'],
      'estate' => $resEstate['fields'],
    );

    print_r($data);
    print_r($fields);
  }

  //Рейтинг востребованности профессий
  function compare ($v1, $v2) {
    if ($v1[1] == $v2[1]) return 0;
    return ($v1[1] > $v2[1])? -1: 1;
  }

  if ($_GET['type'] == 'hh') {

    $string = file_get_contents(__DIR__ . '/data-to-web/hh-regions.json');
  
    $dataReg = json_decode($string, true);

    $data = array();

    foreach ($dataReg as $key => $value) {
      $regionItem = array(
        'region_name' => $value['region'],
        'professions' => array()
      );

      $professionsList = array(
        array('программист', 0),
        array('менеджер', 0),
        array('юрист', 0),
        array('хирург', 0),
        array('фармацевт', 0),
        array('маркетолог', 0),
        array('медик', 0),
        array('фармацевт', 0),
        array('водитель', 0),
        array('IT-специалист', 0),
        array('педагог', 0),
        array('hr', 0),
        array('эколог', 0),
        array('токарь', 0),
        array('слесарь', 0),
        array('инженер', 0),
        array('консультант', 0),
        array('продавец', 0),
        array('бухгалтер', 0),
        array('фармацевт', 0),
      );

      $professionsListCodes = array(
        'программист' => 'programmer',
        'менеджер' => 'manager',
        'юрист' => 'lawyer',
        'хирург' => 'surgeon',
        'фармацевт' => 'pharmacist',
        'маркетолог' => 'marketer',
        'медик' => 'medic',
        'водитель' => 'driver',
        'IT-специалист' => 'IT-specialist',
        'педагог' => 'teacher',
        'hr' => 'hr',
        'эколог' => 'ecologist',
        'токарь' => 'turner',
        'слесарь' => 'locksmith',
        'инженер' => 'ingener',
        'консультант' => 'consultant',
        'продавец' => 'seller',
        'бухгалтер' => 'accountant',
      );

      for ($i = 0; $i < count($professionsList); $i++) {
        
        $res = $db->query(
          "SELECT 
            count(*) as count,
            sum(salary_from) / count(*) as average_salary,
            region_id
          FROM 
            `professions` 
          WHERE 
            `region_hh_id` = ?i AND 
            `profession_name` LIKE '%?s%' AND `salary_from` > 0",
            $value['code'],
            $professionsList[$i][0]
        );
    
        if ($res->getNumRows() > 0) {
          $row = $res->fetch_assoc();
          $professionsList[$i][1] = $row['count'];
          $professionsList[$i][2] = ceil($row['average_salary']);
          $professionsList[$i][3] = ceil($row['region_id']);
        }
      }

      usort($professionsList, "compare");
      
      $professionsList = array_slice($professionsList, 0, 5);

      $newProfList = array();

      for ($i = 0; $i < count($professionsList); $i++) {
        $key = $professionsListCodes[$professionsList[$i][0]];
        
        $newProfList[$key]['name'] = $professionsList[$i][0];
        $newProfList[$key]['count'] = $professionsList[$i][1];
        $newProfList[$key]['average_salary'] = $professionsList[$i][2];
      }

      $regionItem['region_id'] = $professionsList[0][3];
      $regionItem['professions'] = $newProfList;      
      
      array_push($data, $newProfList);
    }

    $fields = array(
      'region_name' => array(
        'label' => 'Регион',
        'unit' => ''
      ),
      'region_id' => array(
        'label' => 'id региона',
        'unit' => ''
      ),
      'professions' => array(
        'name' => array(
          'label' => 'Название профессии',
          'unit' => ''
        ),
        'count' => array(
          'label' => 'Количество вакансий',
          'unit' => 'шт.'
        ),
        'average_salary' => array(
          'label' => 'Средняя зарплата',
          'unit' => 'руб.'
        )
      )
    );
    
    /*
      $data => array(
        [0] => array(
          'region_name' => string,
          'professions' => array(
            [0] => array(
              string,
              int,
              int //average-salary
            )
          )
        )
      )
    */
  }


  if ($_GET['type'] == 'serive') {
    
    //запись регионов  
    /*
    $res = $db->query(
      "SELECT * FROM `geo_regions`"
    );

    while ($row = $res->fetch_assoc()) {

      $db->query(
        "UPDATE `professions` set region_id = ?i WHERE `region_name` LIKE '%?s%'",
        $row['id'],
        $row['name']
      );
    }
    */
    
  }


  /* ---------------- UNSERT API ---------------------*/

  //Средняя цена за квадратный метр
  if ($_GET['type'] == 'insert' && $_GET['data'] == 'criminal') {

    $db->query(
      "INSERT INTO `problems` (`id`, `region_id`, `region_name`, `year`, `type_id`) VALUES (NULL, '0', '?s', '?i', '2');", 
      $_GET['region_name'],
      $_GET['year']
    );

    $res = $db->query(
      "SELECT * FROM `problems` ORDER by id desc limit 1"
    );

    $row = $res->fetch_assoc();

    $db->query(
      "INSERT INTO `indexes` (`id`, `problem_id`, `index_id`, `value`) VALUES (NULL, '?i', '1', '?s');", 
      $row['id'],
      $_GET['value']
    );

    $data['problem_id'] = $row['id'];

  }


  $req = array('success' => true, 'data' => $data, 'fields' => $fields);

  echo json_encode($req);
?>