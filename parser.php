<?php
  
  require(__DIR__ . '/config.php');
  
  require_once(__DIR__ . '/lib/Mysql/Statement.php');
  require_once(__DIR__ . '/lib/Mysql/Exception.php');
  require_once(__DIR__ . '/lib/Mysql/Mysql.php');

  use Krugozor\Database\Mysql\Mysql as Mysql;

  $db = Mysql::create($opts['host'], $opts['login'], $opts['password'])
      // Выбор базы данных
      ->setDatabaseName($opts['db'])
      // Выбор кодировки
      ->setCharset("utf8");

  
  // Получаем данные (в виде ассоциативного массива, например)
  //$data = $result->fetch_assoc();

  // Не работает запрос? Не проблема - выведите его на печать:
  //echo $db->getQueryString();
  
  /*
    Типы проблем:
    1 - ДТП
    2 - Преступления
    3 - Демография 
    4 - Экология
  */

  $parse = array(
    'dtp' => false,
    'criminal' => false,
    'dem' => false,
    'ec' => false,
    'salary' => false,
    'real-estate' => false,
    'hh' => false
  );

  //дтп
  if ($parse['dtp']) {
    $string = file_get_contents(__DIR__ . '/data-to-web/dtp-2018.json');
  
    $data = json_decode($string, true);

    $res = $db->query(
      "SELECT * FROM `problems` ORDER by id desc limit 1"
    );

    $row = $res->fetch_assoc();

    foreach ($data as $key => $value) {
    
      $db->query(
        "INSERT INTO `problems` (`id`, `region_id`, `region_name`, `year`, `type_id`) VALUES (NULL, '?i', '?s', '2018', '1');", 
        $value['reg_code'],
        $value['reg_name']
      );

      $res = $db->query(
        "SELECT * FROM `problems` ORDER by id desc limit 1"
      );

      $row = $res->fetch_assoc();

      $db->query(
        "INSERT INTO `indexes` (`id`, `problem_id`, `index_id`, `value`) VALUES (NULL, '?i', '1', '?i');", 
        $row['id'],
        $value['victims_amount']
      );

      $db->query(
        "INSERT INTO `indexes` (`id`, `problem_id`, `index_id`, `value`) VALUES (NULL, '?i', '2', '?i');", 
        $row['id'],
        $value['vehicles_amount']
      );

      $db->query(
        "INSERT INTO `indexes` (`id`, `problem_id`, `index_id`, `value`) VALUES (NULL, '?i', '3', '?i');", 
        $row['id'],
        $value['participants_amount']
      );
    }
  }

  //Вставляем преступность
  if ($parse['criminal']) {
    $string = file_get_contents(__DIR__ . '/data-to-web/criminal-all.json');
  
    $data = json_decode($string, true);

    $res = $db->query(
      "SELECT * FROM `problems` ORDER by id desc limit 1"
    );

    $row = $res->fetch_assoc();

    foreach ($data as $key => $value) {
    
      $db->query(
        "INSERT INTO `problems` (`id`, `region_id`, `region_name`, `year`, `type_id`) VALUES (NULL, '0', '?s', '?i', '2');", 
        $value['place'],
        $value['year']
      );

      $res = $db->query(
        "SELECT * FROM `problems` ORDER by id desc limit 1"
      );

      $row = $res->fetch_assoc();

      $db->query(
        "INSERT INTO `indexes` (`id`, `problem_id`, `index_id`, `value`) VALUES (NULL, '?i', '1', '?s');", 
        $row['id'],
        $value['count']
      );
    }
  }

  //Вставляем демографию
  if ($parse['dem']) {
    $string = file_get_contents(__DIR__ . '/data-to-web/demography.json');
  
    $data = json_decode($string, true);

    $res = $db->query(
      "SELECT * FROM `problems` ORDER by id desc limit 1"
    );

    $row = $res->fetch_assoc();

    foreach ($data as $key => $value) {
    
      $db->query(
        "INSERT INTO `problems` (`id`, `region_id`, `region_name`, `year`, `type_id`) VALUES (NULL, '0', '?s', '?i', '3');", 
        $value['place'],
        $value['year']
      );

      $res = $db->query(
        "SELECT * FROM `problems` ORDER by id desc limit 1"
      );

      $row = $res->fetch_assoc();

      $db->query(
        "INSERT INTO `indexes` (`id`, `problem_id`, `index_id`, `value`) VALUES (NULL, '?i', '1', '?s');", 
        $row['id'],
        $value['count']
      );
    }
  }

  //экология
  if ($parse['ec']) {
    $string = file_get_contents(__DIR__ . '/data-to-web/eco.json');
  
    $data = json_decode($string, true);

    $res = $db->query(
      "SELECT * FROM `problems` ORDER by id desc limit 1"
    );

    $row = $res->fetch_assoc();

    foreach ($data as $key => $value) {
    
      $db->query(
        "INSERT INTO `problems` (`id`, `region_id`, `region_name`, `year`, `type_id`) VALUES (NULL, '?i', '?s', '2018', '4');", 
        0,
        $value['regin_name']
      );

      $res = $db->query(
        "SELECT * FROM `problems` ORDER by id desc limit 1"
      );

      $row = $res->fetch_assoc();

      $db->query(
        "INSERT INTO `indexes` (`id`, `problem_id`, `index_id`, `value`) VALUES (NULL, '?i', '1', '?i');", 
        $row['id'],
        $value['raiting']
      );

      $db->query(
        "INSERT INTO `indexes` (`id`, `problem_id`, `index_id`, `value`) VALUES (NULL, '?i', '2', '?i');", 
        $row['id'],
        $value['sv_index']
      );

      $db->query(
        "INSERT INTO `indexes` (`id`, `problem_id`, `index_id`, `value`) VALUES (NULL, '?i', '3', '?i');", 
        $row['id'],
        $value['pr_index']
      );

      $db->query(
        "INSERT INTO `indexes` (`id`, `problem_id`, `index_id`, `value`) VALUES (NULL, '?i', '4', '?i');", 
        $row['id'],
        $value['prom_index']
      );

      $db->query(
        "INSERT INTO `indexes` (`id`, `problem_id`, `index_id`, `value`) VALUES (NULL, '?i', '5', '?i');", 
        $row['id'],
        $value['soc_index']
      );
    }
  }

  //зарплата
  if ($parse['salary']) {
    $string = file_get_contents(__DIR__ . '/data-to-web/salary.json');
  
    $data = json_decode($string, true);

    $res = $db->query(
      "SELECT * FROM `problems` ORDER by id desc limit 1"
    );

    $row = $res->fetch_assoc();

    foreach ($data as $key => $value) {
    
      $db->query(
        "INSERT INTO `problems` (`id`, `region_id`, `region_name`, `year`, `type_id`) VALUES (NULL, '?i', '?s', '2018', '5');", 
        0,
        $value['regin_name']
      );

      $res = $db->query(
        "SELECT * FROM `problems` ORDER by id desc limit 1"
      );

      $row = $res->fetch_assoc();

      $db->query(
        "INSERT INTO `indexes` (`id`, `problem_id`, `index_id`, `value`) VALUES (NULL, '?i', '1', '?i');", 
        $row['id'],
        $value['salary']
      );
    }
  }
  
  //недвижимость
  if ($parse['real-estate']) {
    $string = file_get_contents(__DIR__ . '/data-to-web/real-estate.json');
  
    $data = json_decode($string, true);

    $res = $db->query(
      "SELECT * FROM `problems` ORDER by id desc limit 1"
    );

    $row = $res->fetch_assoc();

    foreach ($data as $key => $value) {
    
      $db->query(
        "INSERT INTO `problems` (`id`, `region_id`, `region_name`, `year`, `type_id`) VALUES (NULL, '?i', '?s', '2018', '6');", 
        0,
        $value['regin_name']
      );

      $res = $db->query(
        "SELECT * FROM `problems` ORDER by id desc limit 1"
      );

      $row = $res->fetch_assoc();

      $db->query(
        "INSERT INTO `indexes` (`id`, `problem_id`, `index_id`, `value`) VALUES (NULL, '?i', '1', '?i');", 
        $row['id'],
        $value['price']
      );
    }
  }
  
  //hh regions
  if ($parse['hh']) {
    $string = file_get_contents(__DIR__ . '/data-to-web/hh-regions.json');
  
    $dataReg = json_decode($string, true);

    /*
    $professionsList = array(
      array('программист', 0),
      array('менеджер', 0),
      array('юрист', 0),
      array('хирург', 0),
      array('фармацевт', 0),
      array('маркетолог', 0),
      array('медик', 0),
      array('фармацевт', 0),
      array('водитель', 0),
      array('IT-специалист', 0),
      array('педагог', 0),
      array('hr', 0),
      array('эколог', 0),
      array('токарь', 0),
      array('слесарь', 0),
      array('инженер', 0),
      array('консультант', 0),
      array('продавец', 0),
      array('бухгалтер', 0),
      array('фармацевт', 0),
    );
    */

    //for start here

    foreach ($dataReg as $key => $value) {

      $ch = curl_init();
      $agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)';
      curl_setopt($ch, CURLOPT_USERAGENT, $agent);
      curl_setopt($ch, CURLOPT_URL, "https://api.hh.ru/vacancies?area=" . $value['code'] . "&per_page=100&page=1");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      $output = curl_exec($ch);
      curl_close($ch);

      $vac = json_decode($output, true);

      foreach ($vac['items'] as $item) {
        
        $salaryFrom = -1;
        $salaryTo = -1;

        if (!empty($item['salary'])) {
          if (!empty($item['salary']['from'])) {
            $salaryFrom = $item['salary']['from'];
          }

          if (!empty($item['salary']['to'])) {
            $salaryTo = $item['salary']['to'];
          }
        }

        $db->query(
          "INSERT INTO `professions` (`id`, `region_hh_id`, `region_name`, `profession_name`, `salary_from`, `salary_to`) VALUES (NULL, '?i', '?s', '?s', '?i', '?i');", 
          $value['code'],
          $value['region'],
          $item['name'],
          $salaryFrom,
          $salaryTo
        );
      }

    }

    //for end here

    
    /*
    for ($i = 0; $i < count($professionsList); $i++) {
      // Искомое значчение
      $int = 0;

      $srch = $professionsList[$i][0];

      array_filter($vac['items'], function($a) use (&$int, $srch) {
        print_r($a);
        return (array_search($srch, $a)) ? ++$int : false;
      });
      
      // Количество совпадений
      $professionsList[$i][1] = ($int ? $int : 0);
    }

    print_r($professionsList);
    */

    //print_r($vac['items']);
  }
?>