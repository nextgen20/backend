# README #

Веб-сервер проекта RADA. 

##  REST API ##

### ДТП ###

**Link:** cyf.c-coin.org/api.php?type=accident

Return:

~~~
{
	success: boolean,
	data: [
		{
      		region_id: int,
      		region_name: string,
      		year: int,
      		victims_amount: number,
      		vehicles_amount: number,
      		participants_amount: number,
			color: int
		}
	],
	feilds: []
}
~~~

### Преступления ###

**Link:** cyf.c-coin.org/api.php?type=crime

Return:

~~~
{
	success: boolean,
	data: [
		{
      		region_name: string,
      		year: int,
      		value: number,
			color: int
		}
	],
	feilds: []
}
~~~

### Демография ###

**Link:** cyf.c-coin.org/api.php?type=demography

Return:

~~~
{
	success: boolean,
	data: [
		{
      		region_name: string,
      		year: int,
      		value: number,
			color: int
		}
	],
	feilds: []
}
~~~


### Экология ###

**Link:** cyf.c-coin.org/api.php?type=eco

Return:

~~~
{
	success: boolean,
	data: [
		{
      		region_name: string,
      		raiting: int, //Место в рейтинге
      		sv_index: int, //Сводный индекс
      		pr_index: int, //Природоохранный индекс
      		prom_index: int, //Промышленный индекс
      		soc_index: int, //Социально-экономический индекс,
			color: int
		}
	],
	feilds: []
}
~~~

### Средняя зарплата ###

**Link:** cyf.c-coin.org/api.php?type=salary

Return:

~~~
{
	success: boolean,
	data: [
		{
      		region_name: string,
      		year: int,
      		value: number
		}
	],
	feilds: []
}
~~~


### Недвижимость ###

**Link:** cyf.c-coin.org/api.php?type=real-estate

Return:

~~~
{
	success: boolean,
	data: [
		{
      		region_name: string,
      		year: int,
      		value: number
		}
	],
	feilds: []
}
~~~


### Работа ###

**Link:** cyf.c-coin.org/api.php?type=hh

Return:

~~~
{
	success: boolean,
	data: [
		{
      region_name: string,
      professions: [
        {
					0: string 
					1: count
					2: average salary
				}
			]
		}
	]
}
~~~

### Средние значения для одного региона ###

**Link:** cyf.c-coin.org/api.php?type=simple-region&region-id=[param]

Return:

~~~
{
	success: boolean,
	data: {
		accident: [],
		crime: [],
		demography: [],
		eco: [],
		salary: [],
		estate: []
	},
	fields: {
		accident: [],
		crime: [],
		demography: [],
		eco: [],
		salary: [],
		estate: []
	}
}
~~~